public class sendEmail {
    
    @future(callout=true)
    public static void SendToCandidate(List<ID> lstId) {
        
        
        List<Candidate_Vikas__c> lstCandidate = [select id,Email__c FROM Candidate_Vikas__c WHERE id IN: lstId];
        system.debug(lstCandidate);
        
        List<Messaging.SingleEmailMessage> mails = new List<Messaging.SingleEmailMessage>();
        Attachment attach = new Attachment();
        for(Candidate_Vikas__c candidate : lstCandidate) {
            
            
            //getting data from VF page
            PageReference pdf = Page.CandidateDetailPDF;
            pdf.getParameters().put('id',candidate.id);
            pdf.setRedirect(true);
            
            Blob b = pdf.getContentAsPDF();
            
            //creating the File Attachment for Mail
            List<Messaging.Emailfileattachment> fileAttachments = new List<Messaging.Emailfileattachment>();
            Messaging.EmailFileAttachment efa = new Messaging.EmailFileAttachment();
            attach.Body=b;
            efa.setFileName('CandidateDetails.pdf');
            efa.setBody(b);
            attach.Name =efa.filename;
    		attach.IsPrivate = false;
          	attach.ParentId=candidate.Id;
          	insert attach;
            fileAttachments.add(efa);
            
            //Creating email structure
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();    
            	List<String> sendTo = new List<String>();
      			sendTo.add(candidate.Email__c);
                mail.setToAddresses(sendTo);
                mail.setSubject('Job Offer!..');  
      			mail.setPlainTextBody( 'Congrats You are Hired' );
            	mail.setFileAttachments(fileAttachments);
                mails.add(mail);
            
        }
        Messaging.sendEmail(mails);    //sending mails
        
    }
}