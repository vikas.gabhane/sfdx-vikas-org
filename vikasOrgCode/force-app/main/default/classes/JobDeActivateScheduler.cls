global class JobDeActivateScheduler implements Schedulable {
    
    global void execute(SchedulableContext sc) {
        //Date presentDay = System.today().date();
        List<Job_Vikas__c> lstJob = new List<Job_Vikas__c>();
        
       
        for(Job_Vikas__c job : [select id,Active1__c,Expires_On__c From Job_Vikas__c WHERE Expires_On__c < TODAY]) {
            job.Active1__c=false;
            lstJob.add(job);
        }
        update lstJob;
    }
    /*
     * JobDeActivateScheduler job = new JobDeActivateScheduler();

	String cronExpr = '0 58 5 * * ?';
	String jId = System.schedule('Job Deactivate4', cronExpr, job);
	*/
}