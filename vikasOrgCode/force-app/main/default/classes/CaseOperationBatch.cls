global class CaseOperationBatch implements Database.Batchable <SObject>{
    //List<Contact> lstContact1 = new List<Contact>();
    //Start Method
    global Database.QueryLocator Start(Database.BatchableContext bc) {
        String Query = 'select Id, Status, Origin, Subject, Owner.type from Case where Status = \'New\' and Origin = \'Phone\'';
        return Database.getQueryLocator(Query);
    }
    //Execute Method
    global void execute(Database.BatchableContext bc, List<Case> lstCase) {
        //system.debug('lstContact in Batch='+lstCase);
        List<Contact> lstContact = new List<Contact>();
        List<Case> lstChildCase = new List<Case>(); 
        Map<Id,User> userMap = new Map<Id, User>([select id, LastName, FirstName from user]);
        //system.debug('userMap = '+userMap);
        for(Case objCase : lstCase){  
            if(objCase.owner.type == 'User'){
                User userObj = userMap.get(objCase.OwnerId);
          //      system.debug('user === '+userObj);
                Contact objContact = new Contact();
                objContact.LastName=userObj.LastName;
                objContact.FirstName=userObj.FirstName;
                lstContact.add(objContact);
            //    System.debug(objContact);
            }else {
               case c =new case();
               c.Status=objCase.Status;
               c.ParentId= objCase.id;   //current parent case id assign(lookup)
               c.Origin=objCase.Origin;
               lstChildCase.add(c);
              // System.debug('c-------'+c);
            }
        } 
        //System.debug('lstContact-------'+lstContact);
        //System.debug('lstChildCase-------'+lstChildCase);
        if(!lstContact.isEmpty()){
            insert lstContact;
        }
        if(!lstChildCase.isEmpty()){
            insert lstChildCase;
        }
    }
    //Finish Method
    global void finish(Database.BatchableContext bc) {
        
        Id BatchId = bc.getJobId();
        system.debug('BatchId::'+ BatchId);
    }
    //To execute
    //Id batchJobId = Database.executeBatch(new CaseOperationBatch(), 20);
    
    
}