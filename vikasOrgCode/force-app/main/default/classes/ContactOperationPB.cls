public class ContactOperationPB {
    
    @InvocableMethod
    public static void populateContactAddress(List<Id> contactId) {
        
        Contact requiredContactData = [SELECT Id,LastModifiedDate,Email,MailingAddress,LastName,Account.Name 
                                       FROM Contact WHERE id In : contactId];
        System.debug('requiredContactData');
        System.debug(requiredContactData);    
        Account requiredAccountData = [Select Id,BillingStreet,BillingCity,BillingState,BillingPostalCode,BillingCountry 
                                       FROM Account WHERE Id=:requiredContactData.AccountId];
        System.debug('requiredAccountData');
        System.debug(requiredAccountData);
        
        requiredContactData.MailingStreet = requiredAccountData.BillingStreet;
        requiredContactData.MailingCountry = requiredAccountData.BillingStreet;
        requiredContactData.MailingState = requiredAccountData.BillingState;
        requiredContactData.MailingCity = requiredAccountData.BillingCity;
        requiredContactData.MailingPostalCode   = requiredAccountData.BillingPostalCode;
        update requiredContactData;
        
        Messaging.SingleEmailMessage sendmail = new Messaging.SingleEmailMessage();
        sendmail.toAddresses = new String[] { requiredContactData.Email };
            //sendmail.optOutPolicy = 'FILTER';
        sendmail.subject = 'Verifying Contacts';
        sendmail.plainTextBody = 'Check Your Contact Details !!'
            				+ 'Country : ' +requiredContactData.MailingCountry
            				+ ' State :'+ requiredContactData.MailingState 
            				+ ' City : '+ requiredContactData.MailingCity 
            				+' Street : '+ requiredContactData.MailingStreet 
            				+' PostalCode : '+ requiredContactData.MailingPostalCode;
        Messaging.SingleEmailMessage[] messages = new List<Messaging.SingleEmailMessage> {sendmail};
            Messaging.SendEmailResult[] results = Messaging.sendEmail(messages);  
    }
    
}