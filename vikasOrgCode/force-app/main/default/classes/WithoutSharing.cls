public without sharing class WithoutSharing {
public List<Candidate_Vikas__c> candidates{get; set;}
   
   public WithoutSharing(WithSharing controller){
       candidateData();
   }
   
   public void candidateData(){
   candidates=[select First_Name__c, Last_Name__c, Email__c, Status__c, Job__c, Age__c, DOB__c, Application_Date__c, Country__c, State__c, Expected_Salary__c  from Candidate_Vikas__c limit 10];                
   }
}