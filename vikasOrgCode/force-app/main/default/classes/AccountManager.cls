@RestResource(urlMapping='/Accounts/*/contacts')
global with sharing class AccountManager {
    @HttpGet
    global static Account getAccount() {
        RestRequest request = RestContext.request;
        // grab the caseId from the end of the URL
        String accountId = request.requestURI.substring(
          request.requestURI.lastIndexOf('/')-18,request.requestURI.lastIndexOf('/') );
        
        List<Account> lstAccount = [select id, Name,(select id, Name from Contacts) FROM Account WHERE id  =: accountId];
        List<Contact> lstContact=[SELECT id,Name FROM contact WHERE account.id = : accountId];
        system.debug('account with contact = '+lstAccount);
        system.debug('contacts = '+lstContact);
        system.debug('** lstAccount[0]= '+ lstAccount[0]);
        
        return lstAccount[0];
    }
}