@RestResource(urlMapping='/FutureCall')
global with sharing class BatchToFutureWebService {
    @HttpPost
    global static void callFuture() {
        
       String requestBody = RestContext.request.requestBody.toString();
       system.debug('requestBody = '+requestBody);
       //Contact lstContact = (Contact)JSON.deserialize(requestBody,Contact.class);

        ContactBatchFutureSupport.updateContactMobilePhone(requestBody);
      
    }
}