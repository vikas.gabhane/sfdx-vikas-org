@isTest
public class JobDeActivateSchedulerTest {
    
    @testSetup
    private static void setup() {
        
        
        Contact contactObj = new Contact (LastName='TestContact');
        insert contactObj;
        Date closeDate = Date.today().addDays(-7);
        
        List<Contact>contactlist = [SELECT id FROM Contact WHERE LastName='TestContact' LIMIT 1];
        
        List<Job_Vikas__c> lstJob1 = new List<Job_Vikas__c>();
        for(Integer i=0;i<100;i++){
            Job_Vikas__c jobObj = new Job_Vikas__c(Name='testJob'+i , Number_of_Positions__c = 3, Job_Type__c='Developer' ,Expires_On__c = System.today().addDays(-7),Active1__c=true,Manager1__c = contactlist[0].Id);
            //System.debug('jobObj=>'+jobObj);
            lstJob1.add(jobObj);
            
        }
        insert lstJob1;
        
        
    }
    @isTest static void testScheduledJob() {
        /*date today = System.today();
        String cronExp = '0 30 15 ? * * *';
        List<Job_Vikas__c> jobListData = new List<Job_Vikas__c>([SELECT Id, Active1__c, Expires_On__c
                                                                    FROM Job_Vikas__c WHERE Expires_On__c<today]);
 
        // Schedule the test job
        String jobId = System.schedule('ScheduledApexTest', cronExp, new JobDeActivateScheduler());
        
        // Verify the scheduled job has not run yet.
        List<Job_Vikas__c> lstJob = [SELECT Id FROM Job_Vikas__c WHERE Id IN: jobListData];
        
        System.assertEquals(100, lstJob.size(), 'Tasks exist before job has run');
        */
        String CRON_EXP = '0 0 0 3 9 ? 2022';
		List<Job_Vikas__c> jobList = [SELECT Id, Active1__c, Expires_On__c FROM Job_Vikas__c WHERE Expires_On__c < TODAY];
       	String jobId = System.schedule('ScheduledApexTest',CRON_EXP,new JobDeActivateScheduler());        
       	CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, NextFireTime FROM CronTrigger WHERE id = :jobId];
       	System.assertEquals(CRON_EXP, ct.CronExpression);
      	System.assertEquals(0, ct.TimesTriggered);  
    	System.assertEquals('2022-09-03 00:00:00', String.valueOf(ct.NextFireTime));
        }
    
}