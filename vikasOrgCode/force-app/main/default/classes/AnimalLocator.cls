public class AnimalLocator {

    public static String getAnimalNameById(Integer animalId) {
        String strResponse ='';
        Http http = new Http();
        HttpRequest request = new HttpRequest();
        request.setEndpoint('https://th-apex-http-callout.herokuapp.com/animals/'+animalId);
        request.setMethod('GET');
        HttpResponse response = http.send(request);
        
        if (response.getStatusCode() == 200) {
            // Deserializes the JSON string into collections of primitive data types.
            Map<String, Object> results = (Map<String, Object>) JSON.deserializeUntyped(response.getBody());
            System.debug(results);
            
            Map<String, Object> animal = (Map<String, Object>)results.get('animal');
            
            strResponse = (string)animal.get('name');
            
            
        }
        
        return strResponse;
        
        
    }
}