@isTest
public class AnimalLocatorTest {
    
    @isTest static void testPostCallout() {
        // Set mock callout class 
        Test.setMock(HttpCalloutMock.class, new AnimalLocatorMock()); 
        String result = AnimalLocator.getAnimalNameById(1);
        String expectedResult='bear';
        System.assertEquals(expectedResult, result);
        
    }
}