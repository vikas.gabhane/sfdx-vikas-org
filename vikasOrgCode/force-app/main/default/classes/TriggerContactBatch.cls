global class TriggerContactBatch implements Database.Batchable <SObject>{
    List<Contact> lstContact1 = new List<Contact>();
    //Start Method
    //public List<Contact> lstContact1 = new List<Contact>(); 
    global Database.QueryLocator Start(Database.BatchableContext bc) {
        String Query = 'Select Id, MobilePhone, Name from Contact where MobilePhone = null';
        return Database.getQueryLocator(Query);
    }
    //Execute Method
    global void execute(Database.BatchableContext bc, List<Contact> lstContact) {
        system.debug('lstContact in Batch='+lstContact);
        if(!lstContact.isEmpty()) {
          	//System.Queueable job = new TriggerContactQueueable(lstContact);
			//System.enqueueJob(job);
            //ContactBatchFutureSupport.NormalMethodToCallFutureMethod(json.serialize(lstContact));
            lstContact1.addAll(lstContact);
            
        }
        /*system.debug('in Batch before calling web service'+lstContact);
        String body = JSON.serialize(lstContact);
        HttpRequest reqOrg = new HttpRequest();
      	reqOrg.setEndpoint('https://enzigma-a7-dev-ed.my.salesforce.com/services/apexrest/FutureCall');
        reqOrg.setMethod('POST');
        reqOrg.setBody(body);
        Http http1 = new Http();
        HttpResponse resOrg =new HttpResponse();
        http1.send(reqOrg);
        system.debug('in Batch after calling web service');
		*/
    }
    //Finish Method
    global void finish(Database.BatchableContext bc) {
        
        Id BatchId = bc.getJobId();
        system.debug('BatchId::'+ BatchId);
        ContactBatchFutureSupport.NormalMethodToCallFutureMethod(lstContact1);
        //ContactBatchFutureSupport.updateContactMobilePhone(Json.serialize(lstContact1));
    }
    //To execute
    //Id batchJobId = Database.executeBatch(new TriggerContactBatch(), 200);
    
}