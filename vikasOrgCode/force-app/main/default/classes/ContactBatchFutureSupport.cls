public class ContactBatchFutureSupport {

    public static void NormalMethodToCallFutureMethod(List<Contact> lstContact) {
        system.debug('lstContact Normal method ='+lstContact);
        String jsonContactString = Json.serialize(lstContact);
        updateContactMobilePhone(jsonContactString);
    }
    
    @future
    public static void updateContactMobilePhone(String jsonContactString) {
        system.debug('String lstContact future method ='+jsonContactString);
        List<Contact> lstContact = (List<Contact>)Json.deserialize(jsonContactString, List<Contact>.class);
        
        system.debug('future contaxt='+lstContact);
        List<Contact> lstContact2= new List<Contact>();
         for(Contact objContact : lstContact) {
                objContact.MobilePhone = '1234';
                lstContact2.add(objContact);
            }
		update lstContact2;
    
    }
}