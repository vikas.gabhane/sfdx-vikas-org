@isTest
private class JobTriggerTest {
    
    @testSetup 
    public static void setUp(){
        
        Contact contact = new Contact(LastName='Gabhane', Manager_Email__c='vikasgabhane14@gmail.com');
        insert contact;
        
        List<Job_Vikas__C> jobList = new List<Job_Vikas__C>();
        for(integer i=0;i<20;i++){
        	jobList.add(new Job_Vikas__C(Name='TestJob'+i,Job_Type__c='Developer',Active1__c=true,Number_Of_Positions__c=6,Manager1__c=contact.Id));
        }
        Database.insert(jobList);
        
        List<Candidate_Vikas__c> candidateList = new List<Candidate_Vikas__c>();
        for(Integer i =0; i<20;i++){
            candidateList.add(new Candidate_Vikas__c(First_Name__c='testFirst'+i,Last_Name__c='testLast'+i,Email__c='vikas.gabhane@enzigma.com',Country__c='India',State__c='Maharashtra',Status__c='Reject',Expected_Salary__c=200,Name__c=jobList[0].Id) );     
        }
        Database.insert(candidateList);
		
    }
    
    @isTest static void checkActiveJobStatusTest(){
        
        Job_Vikas__C job =[select id , Active1__c from Job_Vikas__c Limit 1];
            //new Job_Vikas__c();
        /*job.Name='testJob';
        job.Active1__c=true;
        job.Job_Type__c='Developer';
        job.Number_of_Positions__c=4;
        insert job;*/
        Database.DeleteResult result = Database.delete(job, false);
       	System.assertEquals('This Job is active and can not be deleted',result.getErrors()[0].getMessage());
        
        List<Job_Vikas__c> lstJob=[SELECT id, Active1__c FROM Job_Vikas__c];
        for(Job_Vikas__c jobObj: lstJob){
            if(!jobObj.Active1__c){
                Database.DeleteResult result1 = Database.delete(jobObj, false);
       			System.assertEquals('This Job is active and can not be deleted',result.getErrors()[0].getMessage());
            }
        }
        //List<Job_Vikas__c> lstJob=[SELECT id,Active1__c FROM Job_Vikas__c where Active1__c=:true];
        //system.assertEquals(20, lstJob.size());
        
        
    }
    
    @isTest static void deActivateJobTest(){
        
        List<Job_Vikas__c> jobId=[SELECT id FROM Job_Vikas__c LIMIT 1];
        List<Candidate_Vikas__c> lstCandidate =[SELECT id, Status__c from Candidate_Vikas__c WHERE Name__c=:jobId];
        for(Candidate_Vikas__c candidate : lstCandidate){
            candidate.Status__c = 'Hired';
        }
        Test.startTest();
        update lstCandidate;
        Test.stopTest();
        //system.debug('candidate='+lstCandidate);
        
        List<Job_Vikas__c> lstJob=[SELECT id,Active1__c, Hired_Applicants__c FROM Job_Vikas__c WHERE Active1__c =:false];
        system.assertEquals(1, lstJob.size());
        
    }
        
    @isTest static void activateJobTest(){
        
        List<Job_Vikas__c> jobId=[SELECT id FROM Job_Vikas__c LIMIT 1];
        List<Candidate_Vikas__c> lstCandidate =[SELECT id, Status__c from Candidate_Vikas__c WHERE Name__c=:jobId];
        for(Candidate_Vikas__c candidate : lstCandidate){
            candidate.Status__c = 'Applied';
        }
        Test.startTest();
        update lstCandidate;
        Test.stopTest();
        system.debug('candidate='+lstCandidate);
        
        List<Job_Vikas__c> lstJob=[SELECT id,Active1__c, Hired_Applicants__c FROM Job_Vikas__c WHERE Active1__c =:true];
        system.assertEquals(20,lstJob.size());
        
    }
    
    @isTest static void sendEmailTest(){
        
        List<Job_Vikas__c> jobId=[SELECT id FROM Job_Vikas__c LIMIT 1];
        List<Candidate_Vikas__c> lstCandidate =[SELECT id, Status__c from Candidate_Vikas__c WHERE Name__c=:jobId LIMIT 6];
        for(Candidate_Vikas__c candidate : lstCandidate){
            candidate.Status__c = 'Hired';
        }
        Test.startTest();
        update lstCandidate;
        Test.stopTest();
        //system.debug('candidate='+lstCandidate);
        
        List<Job_Vikas__c> lstJob=[SELECT id,Active1__c, Hired_Applicants__c FROM Job_Vikas__c WHERE Active1__c =:false];
        system.assertEquals(1, lstJob.size());
        
    }
    
}