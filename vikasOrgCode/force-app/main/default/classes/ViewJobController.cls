global class ViewJobController {
    
    
    public Boolean rend { get; set; }
    public Boolean rend1 { get; set; }
    public Boolean candData { get; set; }
    public List<sObject> records{get; set;}
    public List<Candidate_Vikas__c> candidateRecords{get; set;}
    public List<String> fields{get; set;}
    public List<String> candidateFields{get; set;}
    
    public Candidate_Vikas__c candidate { get; private set; }
    public List<Job_Vikas__c> job{get; set;}
    public ViewJobController(){
        
        rend=false;
        rend1=false;
        candData=true;
        Id id= ApexPages.currentPage().getParameters().get('id');
        
        //Id id=selectedJob; //[select id from Job_Vikas__c where Job_Type__c=:selectedJob];
        candidate= new Candidate_Vikas__c();          
    }
    
    public void getData(){
        rend1=true;
        rend=false;
        
        records=[select Name, Job_Type__c,  Number_of_Positions__c, Qualification_Required__c, Required_Skills__c,
                 Salary_Offered__c, Certification_Required__c, Manager1__c  from Job_Vikas__c where id=:selectedJob];
        
        fields= new List<String>{'Name', 'Job_Type__c',  'Number_of_Positions__c', 'Qualification_Required__c', 'Required_Skills__c',
            'Salary_Offered__c', 'Certification_Required__c', 'Manager1__c'};
                }
    
    public PageReference searchFunction() {
        rend=true;
        job= [select Job_Type__c from Job_Vikas__c where id=:selectedJob ];
        candidateRecords=[select First_Name__c, Last_Name__c, Email__c, Status__c, Name__c,Job_Type__c  from Candidate_Vikas__c where Name__c=:job];
        return null;
    }
    
    
    
    public String call_js{get;set;}
    public PageReference save() {
        try{
            System.debug('1'+candidate);
            insert(candidate);
            System.debug('2');
            candData=false;
            searchFunction();
            
        }
        catch(System.DMLexception e){
            
            ApexPages.addMessages(e);
            return null;
        }
        call_js='<script>helloWorld()</script>';
        //PageReference pr= new ApexPages.standardController(candidate).view();
        return null;
    }
    
    //
    public String selectedJob{get;set;}
    public List<Job_Vikas__c> jt{get; set;}
    
    
    public List<SelectOption> getJobOptions() {
        List<SelectOption> jobOptions = new List<SelectOption>();
        jt=[select Job_Type__c,id from Job_Vikas__c];
        
        jobOptions.add(new SelectOption('','-None-'));
        for(Job_Vikas__c j:jt)
        {
            jobOptions.add(new SelectOption(j.id, j.Job_Type__c));
            
        }
        
        return jobOptions;
    }
    
    
   @RemoteAction
   global static void getJob(String jobId) {
       System.debug(jobId);
       Job_Vikas__c job = new Job_Vikas__c();
       job = [SELECT Id, Number_of_Positions__c
                  FROM Job_Vikas__c WHERE Id = :jobId];
       
       job.Number_of_Positions__c = job.Number_of_Positions__c+1;
       system.debug(job);
       update job;
   }
    
}