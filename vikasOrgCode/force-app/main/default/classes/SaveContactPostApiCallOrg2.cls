public class SaveContactPostApiCallOrg2 {
    public static void saveContactInfo(){
        HttpRequest req = new HttpRequest();
        req.setEndpoint('https://enzigmasoftware-4e-dev-ed.my.salesforce.com/services/oauth2/token?grant_type=password&client_id=3MVG9fe4g9fhX0E6W0qhKGntQtV5hT_PkizzQPNHZzj_piw7qja8Qi5bt27E7hZm.wNwrsQhbPHrTEWVIBQmp&client_secret=38D1BB3F0454D838D68A2C17C7997738DC2D9955E4F6E5EB5216455F0B1410E8&username=vikasgabhane@enzigma.com&password=speedstars@123');
        req.setMethod('POST');
        Http http = new Http();
        HttpResponse res =new HttpResponse();
        res = http.send(req);
        system.debug('res get body resp auth');
        system.debug(res.getBody());
        Oauth objOauthInfo = (Oauth)JSON.deserialize(res.getBody(),oauth.class);
        if(objOauthInfo.access_token != null){
            
            Contact objContact = new Contact();
            objContact.FirstName = 'Vikas';
            objContact.LastName = 'Gabhane';
            String body = JSON.serialize(objContact);
            system.debug('after serialize');
            system.debug(body);
            HttpRequest reqOrg = new HttpRequest();
            reqOrg.setEndpoint('https://enzigmasoftware-4e-dev-ed.my.salesforce.com/services/apexrest/saveContactInfo');
            reqOrg.setMethod('POST');
            reqOrg.setBody(body);
            reqOrg.setHeader('Content-Type', 'application/json');
            reqOrg.setHeader('Authorization', 'Bearer '+objOauthInfo.access_token);
            Http http1 = new Http();
            HttpResponse resOrg =new HttpResponse();
            resOrg = http1.send(reqOrg);
            system.debug('contact info');
            system.debug(resOrg.getBody());
            
        }
    }
    public class Oauth{
        public String access_token{get;set;}
        public String instance_url{get;set;}
        public String id{get;set;}
        public String token_type{get;set;}
        public String issued_at{get;set;}
        public String signature{get;set;}
    }
}