public class TriggerContactFuture {
    
    public static void NormalMethodToCallFutureMethod(String jsonContactString) {
        updateAccountPhoneNo(jsonContactString);
    }
    
    @future
    public static void updateAccountPhoneNo(String jsonContactString) {
        
        //deserialize the JSON to the Contact List
        
        List<Contact> lstContact = (List<Contact>)Json.deserialize(jsonContactString, List<Contact>.class);
        
        //Printing the Contact List
        
        System.debug('---lstContact List---'+lstContact);
        
        List<Account> account = new List<Account>();
        
        for(Contact contact : lstContact ){       
            
            Account a1 = new Account(Id = contact.AccountId);
            a1.Phone = contact.MobilePhone;
            account.add(a1);
            
        }
        update account;
        
    }
    
}