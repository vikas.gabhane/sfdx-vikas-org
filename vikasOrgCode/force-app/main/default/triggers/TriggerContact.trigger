trigger TriggerContact on Contact (before insert,before update) {
    
    
    //Using JSON.Serialize method we can convert the account list to Json

    String jsonContactString = json.serialize(Trigger.NEW);

    // Pass the JSON String to the Future method

    TriggerContactFuture.NormalMethodToCallFutureMethod(jsonContactString);
    
    /*List<Account> account = new List<Account>();
    
    for(Contact contact : Trigger.new ){       
            
            Account a1 = new Account(Id = contact.AccountId);
            a1.Phone = contact.MobilePhone;
            account.add(a1);
              
        }
        update account;
    */
    
    
}